

class VideoNet:
    def output:              
        hub_url = "https://tfhub.dev/tensorflow/movinet/a0/base/kinetics-600/classification/3"

        encoder = hub.KerasLayer(hub_url, trainable=True)

        inputs = tf.keras.layers.Input(
            shape=[None, None, None, 3],
            dtype=tf.float32,
            name='image')

        # [batch_size, 600]
        outputs = encoder(dict(image=inputs))

        model = tf.keras.Model(inputs, outputs, name='movinet')

        example_input = tf.ones([1, 8, 172, 172, 3])
        example_output = model(example_input)

        print(example_output)

                         